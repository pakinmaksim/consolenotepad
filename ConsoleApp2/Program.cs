﻿using System;
using System.Collections.Generic;

namespace ConsoleApp2
{ 
    class Program
    {
        static Random rnd = new Random();
        static string[] names = { "Макс", "Вадим", "Емеля", "Сергей", "Даниил", "Петр", "Егор", "Игорь", "Дмитрий", "Александр", "Владислав" };
        static string[] familyNames = { "Серко", "Лукошенко", "Пакин", "Хруствовский", "Исаев", "Путин", "Иванов", "Петров", "Сидоров", "Навальный"};
        static string[] fatherNames = { "Максимович", "Вадимович", "Сергеевич", "Даниилович", "Петрович", "Егорович", "Игоревич", "Дмитриевич", "Александрович"};


        static List<Note> notes = new List<Note>();

        static void Main(string[] args)
        {
            while (true)
            {

                Console.Clear();
                Console.WriteLine($"В вашей записной книжке {notes.Count} контактов.\nВыберите действие:\n 1 - добавить контакт\n 2 - посмотреть информацию о контакте\n 3 - изменить контакт\n 4 - удалить контакт\n 5 - посмотреть всех\n 6 - создать 10 случайных контактов");

                char input = Console.ReadKey().KeyChar;

                switch (input)
                {
                    case '1': Create(); break;
                    case '2': ShowNote(FindNote()); break;
                    case '3': EditMode(FindNote()); break;
                    case '4': Remove(FindNote()); break;
                    case '5': ShowAll(); break;
                    case '6': CreateRandomNotes(10); break;
                    case '0': return;
                }
            }
        }

        static Note FindNote()
        {
            Console.Clear();
            Console.Write("Введите номер контакта:");
            int input = int.Parse(Console.ReadLine());
            return notes[input - 1];
        }

        static void Create()
        {
            Note note = new Note();
            notes.Add(note);
            EditMode(note);
        }

        static void Remove(Note note)
        {
            notes.Remove(note);
        }

        static void EditValue(int input, Note note)
        {
            Console.Clear();
            Console.WriteLine("Изменить на что?");
            string newValue = Console.ReadLine();
            note.properties[input - 1] = newValue;

        }

        static void EditMode(Note note)
        {
            Console.Clear();

            char input;
            while (true)
            {
                Console.WriteLine("Ваш контакт:");
                Console.WriteLine(note);
                Console.WriteLine("0. Сохранить и назад");
                Console.WriteLine("Что вы хотите изменить?");

                input = Console.ReadKey().KeyChar;

                if (input == '0')
                {
                    if (note.isValidNote()) break;

                    Console.Clear();
                    Console.WriteLine("Контакт обязан иметь Имя и Фамилию, а также 11-значиный номер!");
                    continue;
                }

                EditValue(int.Parse(input.ToString()), note);
                Console.Clear();
            }
        }

        static void ShowNote(Note note)
        {
            Console.Clear();
            Console.WriteLine(note);
            Console.ReadLine();
        }

        static void ShowAll()
        {
            Console.Clear();
            for (int i = 0; i < notes.Count; i++)
            {
                Console.WriteLine($"--------{i + 1}--------");
                Console.WriteLine(notes[i].GetSmallInfo());
            }
            Console.ReadLine();
        }

        static void CreateRandomNotes(int n)
        {
            while (n-- > 0)
            {
                notes.Add(CreateRandomNote());
            }
        }

        static Note CreateRandomNote()
        {
            Note note = new Note();
            note.properties[0] = GetRandomString(names);
            note.properties[1] = GetRandomString(familyNames);
            note.properties[2] = GetRandomString(fatherNames);
            note.properties[3] = GetRandomPhone();
            return note;
        }

        static string GetRandomString(string[] arr)
        {
            return arr[rnd.Next(arr.Length)];
        }

        static string GetRandomPhone()
        {
            string phone = "89";
            for (int i = 0; i < 9; i++)
                phone += rnd.Next(10).ToString();
            return phone;
        }
    }


    class Note
    {
        public static string[] propertiesName = { "Имя", "Фамилия", "Отчество", "Номер", "Страна", "День рожденья", "Организация", "Должность", "Заметки" };
        public string[] properties = new string[propertiesName.Length];


        public Note()
        {
            for (int i = 0; i < properties.Length; i++)
            {
                properties[i] = "";
            }
        }
        public bool isValidNote()
        {
            if (properties[0] == "") return false;
            if (properties[1] == "") return false;
            if (properties[3].Length != 11) return false;
            if (!long.TryParse(properties[3], out long n)) return false;

            return true;
        }

        public string GetSmallInfo()
        {
            string info = "";
            int[] need = { 0, 1, 3};
            for (int i = 0; i < need.Length; i++)
                info += $"{i + 1}. {propertiesName[need[i]]}: {properties[need[i]]} \n";
            return info;
        }

        public override string ToString()
        {
            string result = "";
            for (int i = 0; i < propertiesName.Length; i++)
            {
                result += $"{i + 1}. {propertiesName[i]}: {properties[i]}\n";
            }
            return result;
        }

    }
}
